//
//  UIBezierPath+App.h
//  ASGarlandIndicatorDemo
//
//  Created by Artem Shvets on 24.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIBezierPath (App)

+ (UIBezierPath *)bezierPathWithStarInRect:(CGRect)rect;

@end

NS_ASSUME_NONNULL_END
