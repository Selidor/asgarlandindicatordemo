//
//  UIBezierPath+App.m
//  ASGarlandIndicatorDemo
//
//  Created by Artem Shvets on 24.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import "UIBezierPath+App.h"

@implementation UIBezierPath (App)

+ (UIBezierPath *)bezierPathWithStarInRect:(CGRect)rect{
    
    UIBezierPath* starPath = [UIBezierPath bezierPath];
    CGFloat w = rect.size.width;
    CGFloat h = rect.size.height;
    
    [starPath moveToPoint: CGPointMake(w * 0.5, 0)];
    [starPath addLineToPoint: CGPointMake(w * 0.66, h * 0.33)];

    [starPath addLineToPoint: CGPointMake(w, h * 0.36)];
    [starPath addLineToPoint: CGPointMake(w * 0.75, h * 0.6)];

    [starPath addLineToPoint: CGPointMake(w * 0.85, h)];
    [starPath addLineToPoint: CGPointMake(w * 0.5, h * 0.75)];

    [starPath addLineToPoint: CGPointMake(w * 0.15, h)];
    [starPath addLineToPoint: CGPointMake(w * 0.25, h * 0.6)];

    [starPath addLineToPoint: CGPointMake(0, h * 0.36)];
    [starPath addLineToPoint: CGPointMake(w * 0.36, h * 0.33)];
    [starPath closePath];
    
    return starPath;
}

@end
