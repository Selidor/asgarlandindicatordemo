//
//  ViewController.m
//  ASGarlandIndicatorDemo
//
//  Created by Artem Shvets on 23.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import "ViewController.h"
#import "ASGarlandIndicator.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet ASGarlandIndicator *garlandIndicator;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.garlandIndicator setFirstLampColor:[UIColor redColor]];
    [self.garlandIndicator setSecondLampColor:[UIColor yellowColor]];
    [self.garlandIndicator setThirdLampColor:[UIColor greenColor]];
    [self.garlandIndicator start];
}

- (IBAction)makeCircleGarland:(id)sender {
    
    self.garlandIndicator.form = ASGarlandFormCircle;
    [self.garlandIndicator setNeedsLayout];
}
- (IBAction)makeSquareGarland:(id)sender {
    
    self.garlandIndicator.form = ASGarlandFormBorder;
    [self.garlandIndicator setNeedsLayout];
}
- (IBAction)makeStarGarland:(id)sender {
    
    self.garlandIndicator.form = ASGarlandFormStar;
    [self.garlandIndicator setNeedsLayout];
}

@end
