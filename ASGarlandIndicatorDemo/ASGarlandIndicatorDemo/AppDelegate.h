//
//  AppDelegate.h
//  ASGarlandIndicatorDemo
//
//  Created by Artem Shvets on 23.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic, strong) UIWindow *window;

@end

