//
//  ASGarlandIndicator.h
//  ASGarlandIndicatorDemo
//
//  Created by Artem Shvets on 23.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//



#import <UIKit/UIKit.h>

typedef NS_ENUM (NSUInteger, ASGarlandForm) {
    ASGarlandFormBorder = 0,
    ASGarlandFormCircle,
    ASGarlandFormStar
};

NS_ASSUME_NONNULL_BEGIN

@interface ASGarlandIndicator : UIView

@property (nonatomic, strong) IBInspectable UIColor *firstLampColor;
@property (nonatomic, strong) IBInspectable UIColor *secondLampColor;
@property (nonatomic, strong) IBInspectable UIColor *thirdLampColor;
@property (nonatomic) ASGarlandForm form;

- (void)start;
- (void)stop;

@end

NS_ASSUME_NONNULL_END
