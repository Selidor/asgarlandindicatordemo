//
//  ASGarlandIndicator.m
//  ASGarlandIndicatorDemo
//
//  Created by Artem Shvets on 23.12.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

#import "ASGarlandIndicator.h"
#import "UIBezierPath+App.h"

static const CGFloat kLampDashInterval = 90.f;
static const CGFloat kLampDashLength = 1.f;

#define CGFloatMin(__a__, __b__)  fmin((__a__), (__b__))
#define CGFloatMax(__a__, __b__)  fmax((__a__), (__b__))

@interface ASGarlandIndicator ()

@property (nonatomic, strong) CAShapeLayer* firstShapeLampLayer;
@property (nonatomic, strong) CAShapeLayer* secondShapeLampLayer;
@property (nonatomic, strong) CAShapeLayer* thirdShapeLampLayer;

@property (nonatomic, strong) NSArray <CAShapeLayer *>*lamps;
@property (nonatomic, strong) UIBezierPath *path;

@property (nonatomic, weak) NSTimer *animationTimer;
@end
@implementation ASGarlandIndicator

- (void)awakeFromNib{
    
    [super awakeFromNib];
    [self setup];
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if(self){
        [self setup];
    }
    return self;
}


- (void)setup{
    
    self.layer.masksToBounds = NO;
    
    [self.layer addSublayer:self.firstShapeLampLayer];
    [self.layer addSublayer:self.secondShapeLampLayer];
    [self.layer addSublayer:self.thirdShapeLampLayer];
    
    self.lamps = @[self.firstShapeLampLayer, self.secondShapeLampLayer, self.thirdShapeLampLayer];
    [self updateFrames];
    self.form = _form;

}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self updateFrames];
}

- (void)updateFrames{
    
    [self.firstShapeLampLayer setBounds:self.bounds];
    [self.secondShapeLampLayer setBounds:self.bounds];
    [self.thirdShapeLampLayer setBounds:self.bounds];
    
    CGPoint position = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    self.firstShapeLampLayer.position = position;
    self.secondShapeLampLayer.position = position;
    self.thirdShapeLampLayer.position = position;
    
    [self applyForm:_form];
    
    CABasicAnimation *animation = [self createFormAnimation];
    animation.fromValue = (__bridge id _Nullable)(self.firstShapeLampLayer.path);
    animation.toValue = (__bridge id _Nullable)(_path.CGPath);
    
    [self.firstShapeLampLayer setPath:_path.CGPath];
    [self.secondShapeLampLayer setPath:_path.CGPath];
    [self.thirdShapeLampLayer setPath:_path.CGPath];
    
    [self.firstShapeLampLayer addAnimation:animation forKey:@"path"];
    [self.secondShapeLampLayer addAnimation:animation forKey:@"path"];
    [self.thirdShapeLampLayer addAnimation:animation forKey:@"path"];
    
    [self stop];
    [self start];
}

#pragma mark - Actions

- (void)stop{
    
    [self.animationTimer invalidate];
    self.animationTimer = nil;
    
    self.firstShapeLampLayer.lineDashPhase = 0;
    self.secondShapeLampLayer.lineDashPhase = kLampDashInterval / 3;
    self.thirdShapeLampLayer.lineDashPhase = (2 * kLampDashInterval / 3);
}

- (void)start{
    
    __weak typeof(self) weakSelf = self;
    self.animationTimer = [NSTimer scheduledTimerWithTimeInterval:0.3f
                                                          repeats:YES
                                                            block:^(NSTimer * _Nonnull timer) {

        [CATransaction begin];
        [CATransaction setDisableActions:YES];

        CGFloat stepPhase = ((kLampDashInterval + kLampDashLength)/weakSelf.lamps.count);

        weakSelf.firstShapeLampLayer.lineDashPhase += stepPhase;
        weakSelf.secondShapeLampLayer.lineDashPhase += stepPhase;
        weakSelf.thirdShapeLampLayer.lineDashPhase += stepPhase;

        [CATransaction commit];
    }];
}

- (void)applyForm:(ASGarlandForm)form{
    switch (form) {
        case ASGarlandFormBorder:
            [self applyBorderForm];
            break;
        case ASGarlandFormCircle:
            [self applyCircleForm];
            break;
        case ASGarlandFormStar:
            [self applyStarForm];
            break;
        default:
            break;
    }
}

- (void)applyBorderForm{
    _path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:1];
}

- (void)applyCircleForm{
    
    CGFloat diameter = CGFloatMin(self.bounds.size.width, self.bounds.size.height);
    CGRect rect = CGRectMake((self.bounds.size.width - diameter)/2,
                             (self.bounds.size.height - diameter)/2,
                             diameter, diameter);
    
    _path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:diameter/2];
}

- (void)applyStarForm{
    
    _path = [UIBezierPath bezierPathWithStarInRect:self.bounds];
}

#pragma mark - Setters

- (void)setForm:(ASGarlandForm)form{
    
    if(_form != form){
        _form = form;
    }
    
    [self applyForm:form];
}

- (void)setFirstLampColor:(UIColor *)firstLampColor{
    
    _firstLampColor = firstLampColor;
    self.firstShapeLampLayer.strokeColor = firstLampColor.CGColor;
}

- (void)setSecondLampColor:(UIColor *)secondLampColor{
    
    _secondLampColor = secondLampColor;
    self.secondShapeLampLayer.strokeColor = secondLampColor.CGColor;
}

- (void)setThirdLampColor:(UIColor *)thirdLampColor{
    
    _thirdLampColor = thirdLampColor;
    self.thirdShapeLampLayer.strokeColor = thirdLampColor.CGColor;
}

- (CABasicAnimation *)createFormAnimation{
    
    CABasicAnimation *shrinkStart = [CABasicAnimation animationWithKeyPath:@"path"];
    shrinkStart.beginTime = 0.f;
    shrinkStart.duration = 0.75;
    shrinkStart.additive = YES;
    shrinkStart.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    return shrinkStart;
}

#pragma mark - Getters

- (CAShapeLayer *)firstShapeLampLayer{
    
    if(!_firstShapeLampLayer){
        _firstShapeLampLayer = [self createLampLayer];
    }
    return _firstShapeLampLayer;
}

- (CAShapeLayer *)secondShapeLampLayer{
    
    if(!_secondShapeLampLayer){
        _secondShapeLampLayer = [self createLampLayer];
        _secondShapeLampLayer.lineDashPhase = kLampDashInterval / 3;
    }
    return _secondShapeLampLayer;
}

- (CAShapeLayer *)thirdShapeLampLayer{
    
    if(!_thirdShapeLampLayer){
        _thirdShapeLampLayer = [self createLampLayer];
        _thirdShapeLampLayer.lineDashPhase = (2 * kLampDashInterval / 3);
    }
    return _thirdShapeLampLayer;
}

- (CAShapeLayer *)createLampLayer{
    
    CAShapeLayer *lampLayer = [CAShapeLayer layer];
    lampLayer.backgroundColor = [UIColor clearColor].CGColor;
    lampLayer.fillColor = [UIColor clearColor].CGColor;
    lampLayer.strokeColor = [UIColor whiteColor].CGColor;
    CGFloat diameter = 20.f;
    lampLayer.lineWidth = diameter;
    lampLayer.lineJoin = kCALineJoinRound;
    lampLayer.lineCap = kCALineCapRound;
    lampLayer.lineDashPattern = @[@(kLampDashLength),@(kLampDashInterval),@(kLampDashLength),@(kLampDashInterval)];
    
    return lampLayer;
}

@end
